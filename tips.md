docker login -u user -p $TOKEN  docker.io

# docker build -t davidaparicio/woodstoredashboard:2.0.0 . --no-cache=true --platform=linux/amd64

```bash
dest_cr=docker.io
for img in $(grep 'IMG' .env |tr -d '\r'| cut -d'=' -f2); do
    echo move ${img} to ${dest_cr}
    docker image tag ${img} ${dest_cr}/${img}
    docker image push ${dest_cr}/${img}
done
```

# docker build -t davidaparicio/documentation:2.0.0 . --no-cache=true
# docker run -d -p 1337:80 --name doc davidaparicio/documentation:2.0.0