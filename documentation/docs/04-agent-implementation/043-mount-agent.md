---
sidebar_position: 0
sidebar_label: Mount agent
title: Mount agent
---

## Mount agent

- You have to mount the jar file corresponding to the agent in your image ( jar under the folder bin)
- The agent must be put on the following services (lumbercamp, furniturestore-1, furniturestore-2, furniturestore-3)
- You have to use the "volumes" part of the docker-compose

```yml
apps:
    container_name: apps
    volumes:
        - SOURCE:DESTINATION
```
