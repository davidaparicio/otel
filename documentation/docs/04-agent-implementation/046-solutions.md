---
sidebar_position: 0
sidebar_label: Solutions
title: Solutions
---

```yml
lumbercamp :
    container_name: lumbercamp
    build: 
        dockerfile: Dockerfile
        context: ./src/lumbercamp
    volumes:
        - ./bin/${JAVAAGENT_NAME}:/tools/opentelemetry-javaagent.jar
    ports:
        - 8080:8080
    environment:
        SPRING_OPTS: --spring.datasource.url=jdbc:postgresql://postgres:5432/postgres
        JAVA_TOOL_OPTIONS: -javaagent:/tools/opentelemetry-javaagent.jar
        OTEL_EXPORTER_OTLP_ENDPOINT: http://otel-collector:10001
        OTEL_SERVICE_NAME: lumbercamp
        OTEL_LOGS_EXPORTER: otlp
        OTEL_METRICS_EXPORTER: otlp
        OTEL_TRACES_EXPORTER: otlp
    depends_on:
        - otel-collector

furniturestore-1:
    container_name: furniturestore-1
    build: 
        dockerfile: Dockerfile
        context: ./src/furniturestore
    volumes:
        - ./bin/${JAVAAGENT_NAME}:/tools/opentelemetry-javaagent.jar
    environment:
        SPRING_OPTS: --app.woodshop.url=http://lumbercamp:8080
        JAVA_TOOL_OPTIONS: -javaagent:/tools/opentelemetry-javaagent.jar
        OTEL_EXPORTER_OTLP_ENDPOINT: http://otel-collector:10001
        OTEL_SERVICE_NAME: furniturestore-1
        OTEL_LOGS_EXPORTER: otlp
        OTEL_METRICS_EXPORTER: otlp
        OTEL_TRACES_EXPORTER: otlp
    depends_on:
        - otel-collector
        - postgres
        - lumbercamp

furniturestore-2:
    container_name: furniturestore-2
    build: 
        dockerfile: Dockerfile
        context: ./src/furniturestore
    volumes:
        - ./bin/${JAVAAGENT_NAME}:/tools/opentelemetry-javaagent.jar
    environment:
        SPRING_OPTS: --app.woodshop.url=http://lumbercamp:8080
        JAVA_TOOL_OPTIONS: -javaagent:/tools/opentelemetry-javaagent.jar
        OTEL_EXPORTER_OTLP_ENDPOINT: http://otel-collector:10001
        OTEL_SERVICE_NAME: furniturestore-2
        OTEL_LOGS_EXPORTER: otlp
        OTEL_METRICS_EXPORTER: otlp
        OTEL_TRACES_EXPORTER: otlp
    depends_on:
        - otel-collector
        - postgres
        - lumbercamp

furniturestore-3:
    container_name: furniturestore-3
    build: 
        dockerfile: Dockerfile
        context: ./src/furniturestore
    volumes:
        - ./bin/${JAVAAGENT_NAME}:/tools/opentelemetry-javaagent.jar
    environment:
        SPRING_OPTS: --app.woodshop.url=http://lumbercamp:8080
        JAVA_TOOL_OPTIONS: -javaagent:/tools/opentelemetry-javaagent.jar
        OTEL_EXPORTER_OTLP_ENDPOINT: http://otel-collector:10001
        OTEL_SERVICE_NAME: furniturestore-3
        OTEL_LOGS_EXPORTER: otlp
        OTEL_METRICS_EXPORTER: otlp
        OTEL_TRACES_EXPORTER: otlp
    depends_on:
        - otel-collector
        - postgres
        - lumbercamp
```