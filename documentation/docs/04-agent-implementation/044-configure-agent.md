---
sidebar_position: 0
sidebar_label: Configure agent
title: Configure agent
---

- You have to set the environment variables in order to configure the agent
  - You have to use the "environment" part of the docker-compose

- The first part consists in defining our agent as a javagent of the jvm
  - The **JAVA_TOOL_OPTIONS** environment variable must be used in configuration as follows: `-javaagent:PATHTOJAR`
  
- Now we just have to fill in the variables for the agent configuration
  - **OTEL_EXPORTER_OTLP_ENDPOINT**: The URL of the OpenTelemetry collector ( <http://otel-collector:10001> )
  - **OTEL_SERVICE_NAME**: the name of the service that will appear in grafana
  - **OTEL_LOGS_EXPORTER**: **otlp** if you want to activate it, **none** otherwise
  - **OTEL_METRICS_EXPORTER**: **otlp** if you want to activate it, **none** otherwise
  - **OTEL_TRACES_EXPORTER**: **otlp** if you want to activate it, **none** otherwise

```yml
apps:
    container_name: apps
    environment:
      VAR1: XXX
      VAR2: XXX
```