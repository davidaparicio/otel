---
sidebar_position: 0
sidebar_label: Check
title: Check
---

:::warning to do first
```bash
git checkout master
```
:::

## Start the application

```bash
docker-compose up -d
```

## View the application

- [Grafana](http://localhost:3000/?orgId=1)
- [Apps](http://localhost:4200/helloWorld)
