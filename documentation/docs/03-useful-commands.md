---
sidebar_position: 0
sidebar_label: Useful Commands
title: Useful Commands
---

## Start the application

```bash
docker-compose up -d
```


## Stop the application

```bash
docker-compose down
```