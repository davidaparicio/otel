---
sidebar_position: 0
sidebar_label: 📅 Programme
title: Programme
---

1. Observability overview
2. Add OpenTelemetry agents on the different backends applications
3. Set up dasbhoards on grafana
