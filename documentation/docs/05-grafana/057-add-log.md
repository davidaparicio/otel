---
sidebar_position: 0
sidebar_label: Add log view
title: Add log view
---

We will now add a widget to view the logs

You'll need to enter the following information 

- In the top right-hand corner, select *Logs**
- Choose **loki** as the data source
- Switch to **Code mode** for the query

```txt
{exporter="OTLP"} | json | line_format "{{ printf \"%-20.20s\" .application}}\t{{.level}}\t{{.body}}"
```

- You can change the widget title

![ ](img/widget_logs.png)

Then click on save next to apply.
