---
sidebar_position: 0
sidebar_label: Grafana Explorer
title: Grafana Explorer
---

The explore part is useful to check all the metrics, logs, traces at your disposal and build your queries.

It will provide an interface to type the query or a builder that will help you to get the information you need.

For example if we start on the metrics part, we will have a "Metrics browser" part that we can filter according to the labels in order to provide a list of available metrics for our application.

![ ](img/grafana_explore_help_metrics.png)

In our case we know that the name of our application is on the label **job** so we will filter on **job** and have the list of all metrics that concern one of the applications.

![ ](img/grafana_explore_metrics_browser.png)

On the left side we have the whole list, we just have to choose the one that can be useful.