---
sidebar_position: 0
sidebar_label: Add CPUmetrics
title: Add CPU metrics
---

You'll need to enter the following information

- In the top right-hand corner, select *Logs**
- Choose **mimir** as the data source
- Switch to **Code mode** for the query

```txt
process_runtime_jvm_cpu_utilization_ratio
```

- You can change the widget title

![ ](img/widget-cpu.png)

Then click on save next to apply.
