---
sidebar_position: 0
sidebar_label: Add Datasource
title: Add Datasource
---

The aim is to add a postgres datasource in order to track inventory via Grafana.

To add a datasource, click on connections>Add new connection

![ ](img/add_datasource.png)

You can then filter on the postgres database

![ ](img/postgres_datasource.png)

Then click on "Add new Datasource"

![ ](img/postgres_datasource_add.png)

The following fields must be completed

- Host : postgres:5432
- Database : postgres
- User : jack
- Password : secret
- TLS/SSL Mode : disable

Then click on "Save & tests"

![ ](img/datasource_save_test.png)