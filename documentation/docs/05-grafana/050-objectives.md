---
sidebar_position: 0
sidebar_label: Objectives
title: Objectives
---

The aim of this part is to aggregate several data on the same dasbhoard, so we'll find :

- A view of the wood stock
- Real-time logs
- CPU and memory metrics
