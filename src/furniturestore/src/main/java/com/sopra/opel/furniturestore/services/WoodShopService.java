package com.sopra.opel.furniturestore.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.util.retry.Retry;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Service
@Slf4j
public class WoodShopService {

  private List<String> woodTypes = new ArrayList<>();
  @Value("${app.woodshop.url}")
  private String hostApiUrl;

  @PostConstruct
  private void initWoodTypes(){
    WebClient client = WebClient.builder()
            .baseUrl(hostApiUrl)
            .defaultCookie("cookieKey", "cookieValue")
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .defaultUriVariables(Collections.singletonMap("url", hostApiUrl))
            .build();
    client.get().uri("/api/stock/types").exchangeToMono(clientResponse -> clientResponse.bodyToMono(List.class).retryWhen(Retry.fixedDelay(3, Duration.ofSeconds(5)).jitter(0.75))).subscribe(this::addType);
  }

  public void addType(List<String> t){
    this.woodTypes.addAll(t);
  }

  public void askFor(String storeName) {
    Random r = new Random();
    String woodType="oak";
    if(woodTypes.size()>0) {
      woodType = woodTypes.get(r.nextInt(0,woodTypes.size()));
    }

    log.info("Ask for {}", woodType);
    WebClient client = WebClient.builder()
            .baseUrl(hostApiUrl)
            .defaultCookie("cookieKey", "cookieValue")
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .defaultUriVariables(Collections.singletonMap("url", hostApiUrl))
            .build();
    client.post().uri("/order").bodyValue("{\"customer\":\""+storeName+"\",\"type\":\""+woodType+"\",\"quantity\":\""+r.nextInt(70)+"\"}").exchangeToMono(clientResponse -> clientResponse.bodyToMono(String.class)).subscribe(log::info);

  }


}
